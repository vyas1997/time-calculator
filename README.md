## How to install the project

1. Download Visual Studio 2019 Professional setup.
2. Follow the instructions to install Visual Studio.

# Workloads required for this project

1. ASP.NET and web development.
2. .NET Desktop development.
3. Data storage and Processing.
4. .NET Core cross-platform development.

## How to use the project

1. Open the .sln file in Visual Studio
2. Build and run it.

## Note- License used

I used the GNU General Public License version 2 because unlike other licenses the GNU General Public License allows you
 to share and change all versions of a program--to make sure it remains free software for all its users.
