﻿using System;
using System.Collections.Immutable;
using System.Text;
using System.Windows;

namespace TimeCalculator
{
    /// Interaction logic for MainWindow.xaml
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        // below static variables are defined to be immutable because their value should not be changed
        // during execution
        private static ImmutableArray<uint>numOfSecondsPerTimeUnit =
            ImmutableArray.Create(new uint[] { 86400/* secondsperday */, 3600, 60, 1 });
        private static ImmutableArray<string> descriptionOfTimeUnit =
            ImmutableArray.Create(new string[] { " day", " hour", " minute", " second" });
        private void btnCalculateTime_Click(object sender, RoutedEventArgs e)
        {
            const int bufferSize = 128;
            
            try
            {
                lblDisplayTime.Content = "nil";
                // we are using uint as we are assuming that inputed number of seconds can not be negative
                uint inputNumOfSeconds = uint.Parse(txtEnteredSeconds.Text);
                if (inputNumOfSeconds == 0)
                {
                    throw new ArgumentException(
                        "Invalid input: number of seconds cannot be equal to zero!");
                }
                uint numOfRemainingSeconds = inputNumOfSeconds;
                StringBuilder content = new StringBuilder("", bufferSize);
                for (short i = 0; i < numOfSecondsPerTimeUnit.Length && numOfRemainingSeconds > 0; ++i)
                {
                    if (numOfRemainingSeconds >= numOfSecondsPerTimeUnit[i])
                    {
                        uint numOfTimeUnits = numOfRemainingSeconds / numOfSecondsPerTimeUnit[i];
                        numOfRemainingSeconds %= numOfSecondsPerTimeUnit[i];
                        content.Append(numOfTimeUnits);
                        content.Append(descriptionOfTimeUnit[i]);
                        content.Append(numOfTimeUnits > 1 ? "s " : " ");
                    }
                }
                lblDisplayTime.Content = content.ToString();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(
                    ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch
            {
                MessageBox.Show(
                    "Input should be a number greater than zero and less than or equal to "
                    + System.UInt32.MaxValue, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
